from builtins import range
import numpy as np
from random import shuffle


def softmax_loss_naive(W, X, y, reg):
    """
    Softmax loss function, naive implementation (with loops)

    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
      that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using explicit loops.     #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    n_samples = X.shape[0]
    n_classes = W.shape[1]

    for i in range(n_samples):
        sum_exp_scores = 0
        exp_scores = []
        for c in range(n_classes):
            s = np.math.exp(X[i].dot(W[:, c]))
            exp_scores.append(s)
            sum_exp_scores += s
        loss += -np.math.log(exp_scores[y[i]] / sum_exp_scores)
        dW[:, y[i]] -= X[i]
        for c in range(n_classes):
            dW[:, c] += X[i] * exp_scores[c] / sum_exp_scores

    loss /= n_samples 
    dW /= n_samples
    loss += reg * np.sum(W * W)
    dW += 2 * reg * W

    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
    """
    Softmax loss function, vectorized version.

    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
      that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    n_samples = X.shape[0]
    n_classes = W.shape[1]

    scores = X.dot(W)  # shape (N, C)
    exp_scores = np.exp(scores)  # shape (N, C)
    sum_exp_scores = np.sum(exp_scores, axis=1)  # shape (N,)
    log_sum_exp_scores = np.log(sum_exp_scores)  # shape (N,)
    sample_losses = log_sum_exp_scores - scores[np.arange(n_samples), y]  # shape (N,)
    loss += np.mean(sample_losses)
    loss += reg * np.sum(W * W)

    dW += X.T.dot(exp_scores / sum_exp_scores[:, np.newaxis])  # shape (D, C)
    class_match = (y == np.arange(n_classes).reshape([n_classes, 1]))  # shape (C, N)
    dW -= class_match.dot(X).T  # shape (D, C)
    dW /= n_samples
    dW += 2 * reg * W

    # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

    return loss, dW
