"""
Show instability of softmax function.
Softmax has property softmax(x + c) = softmax(x).
This property is used to prevent instability.

Note:
    Strange stuff.
    One time this code works as expected.
    Next times it doesn't.
"""


import numpy as np


def softmax(x):
    exp = np.exp(x)
    return exp / np.sum(exp)


def stable_softmax(x):
    x = np.array(x)
    exp = np.exp(x - np.max(x))
    return exp / np.sum(exp)


def test(text, x):
    print(text)
    print('x', x)
    print('softmax', softmax(x))
    print('stable_softmax', stable_softmax(x))
    print()


def main():
    x0 = [1, 2, 3]
    test('Test softmax with normal input', x0)

    x1 = [100 * x for x in x0]
    test('Test softmax with large input', x1)

    x2 = [x / 1000 for x in x0]
    test('Test softmax with very small input', x2)


if __name__ == '__main__':
    main()
