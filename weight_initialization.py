import numpy as np
import matplotlib.pyplot as plt


def normal_init_001(fan_in, fan_out):
    return np.random.randn(fan_in, fan_out) * 0.01


def normal_init_1(fan_in, fan_out):
    return np.random.randn(fan_in, fan_out)


def xavier_init(fan_in, fan_out):
    return np.random.randn(fan_in, fan_out) / np.sqrt(fan_in)


def relu_init(fan_in, fan_out):
    return np.random.randn(fan_in, fan_out) / np.sqrt(fan_in / 2)


ACTIVATION = 'relu'
INIT = relu_init


D = np.random.randn(1000, 500)
hidden_layer_sizes = [500] * 10
nonlinearities = [ACTIVATION] * len(hidden_layer_sizes)

act = dict(
    relu=lambda x: np.maximum(0, x),
    tanh=lambda x: np.tanh(x)
)
Hs = {}

for i in range(len(hidden_layer_sizes)):
    X = D if i == 0 else Hs[i - 1]
    fan_in = X.shape[1]
    fan_out = hidden_layer_sizes[i]
    W = INIT(fan_in, fan_out)

    H = np.dot(X, W)
    H = act[nonlinearities[i]](H)
    Hs[i] = H

print('input layer had mean {} and std {}'.format(np.mean(D), np.std(D)))
layer_means = [np.mean(H) for _, H in Hs.items()]
layer_stds = [np.std(H) for _, H in Hs.items()]
print(layer_means, layer_stds)
for i, H in Hs.items():
    print('hidden layer {} had mean {} and std {}'.format(i + 1, layer_means[i], layer_stds[i]))

plt.figure()
plt.subplot(121)
plt.plot(list(Hs.keys()), layer_means, 'ob-')
plt.title('layer mean')
plt.subplot(122)
plt.plot(list(Hs.keys()), layer_stds, 'or-')
plt.title('layer std')

plt.figure()
for i, H in Hs.items():
    plt.subplot(1, len(Hs), i + 1)
    plt.hist(H.ravel(), 30, [-1, 1])

plt.show()
